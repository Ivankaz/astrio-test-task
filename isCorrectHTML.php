<?php
// получение имени и типа тега
/**
 * @param string $tag
 * @return array|false
 */
function getInfoTag(string $tag)
{
    // определяю открытый тег
    preg_match('/^<([a-z]*)>$/', $tag, $matches);
    if ($matches) {
        return [
            'name' => $matches[1],
            'type' => 'open',
            ];
    }

    // определяю закрытый тег
    preg_match('/^<\/([a-z]*)>$/', $tag, $matches);
    if ($matches) {
        return [
            'name' => $matches[1],
            'type' => 'close',
            ];
    }

    return false;
}

// получение непрерывной последовательности тегов (открывающихся или закрывающихся)
/**
 * @param array $tags
 * @param string $typeTags
 * @return array
 */
function getSequenceTags(array &$tags, string $typeTags): array
{
    // последовательность тегов
    $sequenceTags = [];
    // ключ тега
    $i = 0;
    // имя и тип тега
    $infoTag = getInfoTag($tags[$i]);

    // выбираю непрерывную последовательность тегов с типом $typeTags
    while ($infoTag && $infoTag['type'] == $typeTags) {
        $sequenceTags[] = $infoTag['name'];

        // следующий тег
        $nextTag = $tags[++$i] ?? null;
        // если нет следующего тега, выхожу из цикла
        if (is_null($nextTag)) break;

        // имя и тип тега
        $infoTag = getInfoTag($nextTag);
    }

    // удаляю из массива теги, которые уже распарсил
    array_splice($tags, 0, $i);

    return $sequenceTags;
}

// проверка корректности последовательности тегов HTML
/**
 * @param array $tags
 * @return bool
 */
function isCorrectHTML(array $tags): bool
{
    // если массив пустой, выходим из функции
    if (empty($tags)) return false;

    // пока что есть пары тегов в переданной последовательности
    while(count($tags) > 1) {
        // открытые теги
        $openTags = getSequenceTags($tags, 'open');
        // закрытые теги
        $closeTags = getSequenceTags($tags, 'close');

        // если последовательность открывающихся тегов не равна реверсу последовательности закрывающихся тегов,
        // то HTML неверный
        if ($openTags != array_reverse($closeTags)) return false;
    }

    return true;
}
