<?php
require_once 'isCorrectHTML.php';

// правильная последовательность тегов
$correctTags = [
    '<a>',
    '<div>',
    '</div>',
    '</a>',
    '<span>',
    '</span>',
];
echo var_dump(isCorrectHTML($correctTags)); // выведет: bool(true)

// неправильная последовательность тегов
$incorrectTags = [
    '<a>',
    '<div>',
    '</a>',
];
echo var_dump(isCorrectHTML($incorrectTags)); // выведет: bool(false)