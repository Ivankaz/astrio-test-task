<?php

// поиск названия категории по её ID
function searchCategory(array $categories, int $id)
{
    foreach ($categories as $category) {
        if ($category['id'] == $id) {
            return $category['title'];
        } elseif (isset($category['children'])) {
            // ищу категорию во вложенных категориях
            $title = searchCategory($category['children'], $id);

            // если нашли категорию, возвращаю её имя
            if ($title != null) {
                return $title;
            }
        }
    }

    return null;
}