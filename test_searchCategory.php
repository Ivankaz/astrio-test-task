<?php
require_once 'searchCategory.php';

$categories = array(
    array(
        "id" => 1,
        "title" => "Обувь",
        'children' => array(
            array(
                'id' => 2,
                'title' => 'Ботинки',
                'children' => array(
                    array('id' => 3, 'title' => 'Кожа'),
                    array('id' => 4, 'title' => 'Текстиль'),
                ),
            ),
            array('id' => 5, 'title' => 'Кроссовки',),
        )
    ),
    array(
        "id" => 6,
        "title" => "Спорт",
        'children' => array(
            array(
                'id' => 7,
                'title' => 'Мячи'
            )
        )
    ),
);

var_dump(searchCategory($categories, 1)); // выведет: Обувь
var_dump(searchCategory($categories, 4)); // выведет: Текстиль
var_dump(searchCategory($categories, 7)); // выведет: Мячи
var_dump(searchCategory($categories, 8)); // выведет: NULL