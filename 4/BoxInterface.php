<?php
// интерфейс хранилища
interface BoxInterface
{
    // установить данные
    public function setData($key, $value);

    // получить данные
    public function getData($key);

    // сохранить данные
    public function save();

    // загрузить данные
    public function load();
}