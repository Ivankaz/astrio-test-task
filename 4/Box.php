<?php
require_once 'FileBox.php';
require_once 'DbBox.php';

class Box
{
    private object $__box;

    public function __construct($box) {
        // получаю объект хранилища
        $class = $box . 'Box';
        $this->__box = $class::getInstance();
    }

    public function setData($key, $value) {
        return $this->__box->setData($key, $value);
    }

    public function getData($key) {
        return $this->__box->getData($key);
    }

    public function save() {
        $this->__box->save();
    }

    public function load() {
        $this->__box->load();
    }
}