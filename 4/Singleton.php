<?php
// одноэлементный шаблон
trait Singleton {
    private static $instance = null;

    // защищаю от создания через new Singleton
    private function __construct() { /* ... @return Singleton */ }
    // защищаю от создания через клонирование
    private function __clone() { /* ... @return Singleton */ }
    // защищаю от создания через unserialize
    private function __wakeup() { /* ... @return Singleton */ }

    public static function getInstance() {
        // если $instance равен null, то создаю новый объект
        return
            self::$instance===null
                ? self::$instance = new static()
                // иначе возвращаю существующий объект
                : self::$instance;
    }
}