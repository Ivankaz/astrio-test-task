<?php
require_once 'Box.php';

// классы хранилищ, которые тестирую
$storageClasses = [
    'File',
    'Db',
];

foreach ($storageClasses as $storageClass) {
    echo 'Проверяю ' . $storageClass . 'Box' . PHP_EOL;

    // создаю экземпляр хранилища данных
    $firstBox = new Box($storageClass);
    // загружаю данные из файла
    $firstBox->load();
    // добавляю значение
    $firstBox->setData('name', 'Linus Torvalds');
    // получаю значение
    echo $firstBox->getData('name') . PHP_EOL; // Выведет: Linus Torvalds
    // пытаюсь получить несуществующее значение
    echo $firstBox->getData('age') . PHP_EOL; // Выведет: Ошибка! Ключ "age" не существует.
    // сохраняю данные
    $firstBox->save();

    // пытаюсь создать второй экземпляр хранилища данных
    $secondBox = new Box($storageClass);
    // без загрузки данных получаю значение по ключу "name",
    // значит работаю с тем же экземпляром класса,
    // который был создан при первой инициализации для $firstBox
    echo $secondBox->getData('name') . PHP_EOL; // Выведет: Linus Torvalds

    echo PHP_EOL;
}