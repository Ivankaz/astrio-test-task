<?php
require_once 'Singleton.php';
require_once 'AbstractBox.php';
require_once './vendor/autoload.php';

class DbBox extends AbstractBox
{
    use Singleton;

    private object $client;

    public function __construct() {
        // подключаюсь к Redis
        $this->client = new Predis\Client();
    }

    public function save()
    {
        $this->client->mset($this->data);
    }

    public function load()
    {
        // в $this->data загружаю ключи и значения из Redis
        $keys = $this->client->keys('*');
        foreach ($keys as $key) {
            $this->data[$key] = $this->client->get($key);
        }
    }
}