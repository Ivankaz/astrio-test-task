<?php
require_once 'BoxInterface.php';

// абстрактный класс хранилища
abstract class AbstractBox implements BoxInterface
{
    // данные
    protected array $data;

    public function setData($key, $value) {
        $this->data[$key] = $value;
    }

    public function getData($key) {
        // если ключ существует в массиве с данными
        if (isset($this->data[$key])) {
            return $this->data[$key];
        } else {
            echo "Ошибка! Ключ \"$key\" не существует.";
            return null;
        }
    }

    abstract public function save();

    abstract public function load();
}