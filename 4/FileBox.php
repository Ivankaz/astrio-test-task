<?php
require_once 'Singleton.php';
require_once 'AbstractBox.php';

class FileBox extends AbstractBox
{
    use Singleton;
    private string $fileName = 'data.json';

    public function save()
    {
        file_put_contents($this->fileName, json_encode($this->data, JSON_UNESCAPED_UNICODE));
    }

    public function load()
    {
        try {
            // если файл не существует, создаю пустой массив
            if (!file_exists($this->fileName)) {
                $this->data = [];
                return;
            }

            // загружаю данные из файла
            $this->data = json_decode(file_get_contents($this->fileName), true);
        } catch (TypeError $e) {
            echo 'Ошибка! Не удалось разобрать файл ' . $this->fileName . PHP_EOL;
            die();
        }
    }
}