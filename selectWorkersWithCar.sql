-- получаю имя и фамилию сотрудника,
-- его детей и автомобили перечисленные через запятую
SELECT first_name AS Имя,
       last_name AS Фамилия,
       GROUP_CONCAT(DISTINCT(child.name) SEPARATOR ', ') AS Дети,
       GROUP_CONCAT(DISTINCT(car.model) SEPARATOR ', ') AS Модель_автомобиля
-- объединяю таблицы worker и car с помощью внутреннего соединения,
-- чтобы оставить только тех работников, у которых есть или была машина
FROM worker
         INNER JOIN car ON worker.id = car.user_id
    -- объединяю с таблицей child с помощью LEFT JOIN,
    -- чтобы при отсутствии детей и при наличии машины,
    -- работник все равно попал в выборку
         LEFT JOIN child ON worker.id = child.user_id
-- группирую записи по ID работника
GROUP BY worker.id;